package com.feed.me.library.models;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class CategoryDetails {
	private long id;
	private String name;
	private Set<FeedModel> feeds = new HashSet<>();
}
