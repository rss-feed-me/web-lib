package com.feed.me.library.models;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FeedDetails extends FeedModel {
	private Set<ArticleDetails> articles = new HashSet<>();
}
