package com.feed.me.library.models;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserModel {

	private long id;
	private String username;
	
	@Builder.Default
	private Set<FeedModel> feeds = new HashSet<>();
	
	@Builder.Default
	private Set<CategoryModel> Categories = new HashSet<>();
	
	@Builder.Default
	private boolean admin = false;
	
	@Override
	public String toString() {
		return "User '" + username + "', id: " + id + ", isAdmin: " + admin;
	}
}
