package com.feed.me.library.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArticleDetails extends ArticleModel {

	boolean read;
}
