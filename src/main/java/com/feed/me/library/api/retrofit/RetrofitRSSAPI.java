package com.feed.me.library.api.retrofit;

import java.util.List;

import com.feed.me.library.api.APICallback;
import com.feed.me.library.api.RSSApi;
import com.feed.me.library.models.ArticleModel;
import com.feed.me.library.models.CategoryDetails;
import com.feed.me.library.models.CategoryModel;
import com.feed.me.library.models.FeedDetails;
import com.feed.me.library.models.FeedModel;
import com.feed.me.library.models.ToggleAdminModel;
import com.feed.me.library.models.UserCredentialsModel;
import com.feed.me.library.models.UserModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitRSSAPI extends RSSApi {
	
	private final APIService service;
	
	public RetrofitRSSAPI(String apiUrl) {
		Retrofit retrofit = new Retrofit.Builder().baseUrl(apiUrl)
				.addConverterFactory(GsonConverterFactory.create())
				.build();
		service = retrofit.create(APIService.class);
	}
	
	/**
	 * Used to allow tests to override service variable
	 */
	protected RetrofitRSSAPI(APIService service) {
		this.service = service;
	}

	@Override
	protected void doLogin(final String username, final String password, final APICallback<String> callback) {
		service.loginUser(new UserCredentialsModel(username, password)).enqueue(new Callback<Void>() {
			
			@Override
			public void onResponse(Call<Void> call, Response<Void> response) {
				callback.onSuccess(response.code(), response.headers().get("Authorization"));
			}
			
			@Override
			public void onFailure(Call<Void> call, Throwable t) {
				callback.onError(t);
			}
		});
	}

	@Override
	public void register(String username, String password, APICallback<Void> callback) {
		service.registerUser(new UserCredentialsModel(username, password)).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void fetchUserList(String token, APICallback<List<UserModel>> callback) {
		service.fetchUserList(token).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void toggleAdmin(String token, long uid, boolean isAdmin, APICallback<Void> callback) {
		service.toggleAdmin(token, new ToggleAdminModel(uid, isAdmin)).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void fetchFeeds(String token, APICallback<List<FeedModel>> callback) {
		service.fetchFeeds(token).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void addFeed(String token, String url, APICallback<FeedModel> callback) {
		service.addFeed(token, url).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void delFeed(String token, long f_id, APICallback<Void> callback) {
		service.delFeed(token, f_id).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void fetchFeed(String token, long f_id, APICallback<FeedDetails> callback) {
		service.fetchFeed(token, f_id).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void fetchUnreadArticles(String token, long f_id, APICallback<List<ArticleModel>> callback) {
		service.fetchUnreadArticle(token, f_id).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void setArticleRead(String token, long a_id, APICallback<Void> callback) {
		service.readArticle(token, a_id).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void fetchCategories(String token, APICallback<List<CategoryModel>> callback) {
		service.fetchCategories(token).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void addCategory(String token, String name, APICallback<CategoryModel> callback) {
		service.addCategory(token, name).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void delCategory(String token, long c_id, APICallback<Void> callback) {
		service.delCategory(token, c_id).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void fetchCategory(String token, long c_id, APICallback<CategoryDetails> callback) {
		service.fetchCategory(token, c_id).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void addFeedToCategory(String token, long c_id, long f_id, APICallback<CategoryModel> callback) {
		service.linkFeedToCategory(token, c_id, f_id).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void delFeedFromCategory(String token, long c_id, long f_id, APICallback<CategoryModel> callback) {
		service.delFeedFromCategory(token, c_id, f_id).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void fetchCategoryFeeds(String token, long c_id, APICallback<List<FeedModel>> callback) {
		service.fetchCategoryFeeds(token, c_id).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void fetchUser(String token, APICallback<UserModel> callback) {
		service.fetchUser(token).enqueue(new CustomCallback<>(callback));
	}

	@Override
	public void deleteUser(String token, long uid, APICallback<Void> callback) {
		service.deleteUser(token, uid).enqueue(new CustomCallback<>(callback));
	}

}
