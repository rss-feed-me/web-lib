package com.feed.me.library.api.retrofit;

import java.util.List;

import com.feed.me.library.models.ArticleModel;
import com.feed.me.library.models.CategoryDetails;
import com.feed.me.library.models.CategoryModel;
import com.feed.me.library.models.FeedDetails;
import com.feed.me.library.models.FeedModel;
import com.feed.me.library.models.ToggleAdminModel;
import com.feed.me.library.models.UserCredentialsModel;
import com.feed.me.library.models.UserModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

	@POST("login")
	Call<Void> loginUser(@Body UserCredentialsModel credentials);
	@POST("public/sign-up")
	Call<Void> registerUser(@Body UserCredentialsModel credentials);
	@GET("user")
	Call<UserModel> fetchUser(@Header("Authorization") String token);
	@GET("admin/user_list")
	Call<List<UserModel>> fetchUserList(@Header("Authorization") String token);
	@POST("admin/toggle_admin")
	Call<Void> toggleAdmin(@Header("Authorization") String token, @Body ToggleAdminModel model);
	@DELETE("admin/user_del")
	Call<Void> deleteUser(@Header("Authorization") String token, @Query("uid") final long uid);
	@GET("user/feeds")
	Call<List<FeedModel>> fetchFeeds(@Header("Authorization") String token);
	@POST("user/feeds")
	Call<FeedModel> addFeed(@Header("Authorization") String token, @Query("url") final String url);
	@DELETE("user/feeds")
	Call<Void> delFeed(@Header("Authorization") String token, @Query("f_id") long f_id);
	@GET("user/feeds/{f_id}")
	Call<FeedDetails> fetchFeed(@Header("Authorization") String token, @Path("f_id") long f_id);
	@GET("user/feeds/{f_id}/unread_articles")
	Call<List<ArticleModel>> fetchUnreadArticle(@Header("Authorization") String token, @Path("f_id") long f_id);
	@PUT("user/feeds/read_article")
	Call<Void> readArticle(@Header("Authorization") String token, @Query("a_id") long a_id);
	@GET("user/category")
	Call<List<CategoryModel>> fetchCategories(@Header("Authorization") String token);
	@POST("user/category")
	Call<CategoryModel> addCategory(@Header("Authorization") String token, @Query("name") String name);
	@DELETE("user/category")
	Call<Void> delCategory(@Header("Authorization") String token, @Query("c_id") long c_id);
	@GET("user/category/{c_id}")
	Call<CategoryDetails> fetchCategory(@Header("Authorization") String token, @Path("c_id") long c_id);
	@PUT("user/category/{c_id}")
	Call<CategoryModel> linkFeedToCategory(@Header("Authorization") String token, @Path("c_id") long c_id, @Query("f_id") long f_id);
	@DELETE("user/category/{c_id}")
	Call<CategoryModel> delFeedFromCategory(@Header("Authorization") String token, @Path("c_id") long c_id, @Query("f_id") long f_id);
	@GET("user/category/{c_id}/feeds")
	Call<List<FeedModel>> fetchCategoryFeeds(@Header("Authorization") String token, @Path("c_id") long c_id);
}
