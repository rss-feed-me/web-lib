package com.feed.me.library.api;

public interface APICallback <T> {

	public void onSuccess(int responseCode, final T response);
	public void onError(final Throwable error);
}
