package com.feed.me.library.api;

import java.util.List;

import com.feed.me.library.models.ArticleModel;
import com.feed.me.library.models.CategoryDetails;
import com.feed.me.library.models.CategoryModel;
import com.feed.me.library.models.FeedDetails;
import com.feed.me.library.models.FeedModel;
import com.feed.me.library.models.UserModel;

import lombok.Getter;

/**
 * RSS API base class
 * @author Mathias Hyrondelle
 *
 */
public abstract class RSSApi {
	
	@Getter
	private String token;

	/**
	 * Login the user
	 * @param username
	 * @param password
	 * @param callback
	 */
	public void login(final String username, final String password, final APICallback<String> callback)
	{
		this.doLogin(username, password, new APICallback<String>() {
			
			@Override
			public void onSuccess(int code, String response) {
				if (code == 200)
					token = response;
				else
					token = null;
				callback.onSuccess(code, response);
			}
			
			@Override
			public void onError(Throwable error) {
				callback.onError(error);
			}
		});
	}
	
	public void fetchUser(final APICallback<UserModel> callback)
	{
		this.fetchUser(token, callback);
	}
	
	public void fetchUserList(final APICallback<List<UserModel>> callback)
	{
		this.fetchUserList(token, callback);
	}
	public void toggleAdmin(long uid, boolean isAdmin, final APICallback<Void> callback)
	{
		this.toggleAdmin(token, uid, isAdmin, callback); 
	}
	public  void fetchFeeds(final APICallback<List<FeedModel>> callback)
	{
		this.fetchFeeds(token, callback);
	}
	public void addFeed(String url, final APICallback<FeedModel> callback)
	{
		this.addFeed(token, url, callback);
	}
	public void delFeed(long f_id, final APICallback<Void> callback)
	{
		this.delFeed(token, f_id, callback);
	}
	
	public void fetchFeed(long f_id, final APICallback<FeedDetails> callback)
	{
		this.fetchFeed(token, f_id, callback);
	}
	
	public void fetchUnreadArticles(long f_id, final APICallback<List<ArticleModel>> callback)
	{
		this.fetchUnreadArticles(token, f_id, callback);
	}
	
	public void setArticleRead(long a_id, final APICallback<Void> callback)
	{
		this.setArticleRead(token, a_id, callback);
	}
	
	public void fetchCategories(final APICallback<List<CategoryModel>> callback)
	{
		this.fetchCategories(token, callback);
	}
	
	public void addCategory(final String name, final APICallback<CategoryModel> callback)
	{
		this.addCategory(token, name, callback);
	}
	
	public void delCategory(long c_id, final APICallback<Void> callback)
	{
		this.delCategory(token, c_id, callback);
	}
	
	public void fetchCategory(long c_id, final APICallback<CategoryDetails> callback)
	{
		this.fetchCategory(token, c_id, callback);
	}
	
	public void addFeedToCategory(long c_id, long f_id, final APICallback<CategoryModel> callback)
	{
		this.addFeedToCategory(token, c_id, f_id, callback);
	}
	
	public void delFeedFromCategory(long c_id, long f_id, final APICallback<CategoryModel> callback)
	{
		this.delFeedFromCategory(token, c_id, f_id, callback);
	}
	public void fetchCategoryFeeds(long c_id, final APICallback<List<FeedModel>> callback)
	{
		this.fetchCategoryFeeds(token, c_id, callback);
	}
	public void deleteUser(long uid, final APICallback<Void> callback)
	{
		this.deleteUser(token, uid, callback);
	}
	
	/**
	 * Implementation provided login
	 * @param username
	 * @param password
	 * @param callback
	 */
	protected abstract void doLogin(final String username, final String password, final APICallback<String> callback);
	
	/**
	 * Register the user
	 * @param username
	 * @param password
	 * @param callback
	 */
	public abstract void register(final String username, final String password, final APICallback<Void> callback);
	
	public abstract void fetchUser(final String token, final APICallback<UserModel> callback);
	public abstract void fetchUserList(final String token, final APICallback<List<UserModel>> callback);
	public abstract void toggleAdmin(final String token, long uid, boolean isAdmin, final APICallback<Void> callback);
	public abstract void deleteUser(final String token, long uid, final APICallback<Void> callback);
	public abstract void fetchFeeds(final String token, final APICallback<List<FeedModel>> callback);
	public abstract void addFeed(final String token, String url, final APICallback<FeedModel> callback);
	public abstract void delFeed(final String token, long f_id, final APICallback<Void> callback);
	public abstract void fetchFeed(final String token, long f_id, final APICallback<FeedDetails> callback);
	public abstract void fetchUnreadArticles(final String token, long f_id, final APICallback<List<ArticleModel>> callback);
	public abstract void setArticleRead(final String token, long a_id, final APICallback<Void> callback);
	public abstract void fetchCategories(final String token, final APICallback<List<CategoryModel>> callback);
	public abstract void addCategory(final String token, final String name, final APICallback<CategoryModel> callback);
	public abstract void delCategory(final String token, long c_id, final APICallback<Void> callback);
	public abstract void fetchCategory(final String token, long c_id, final APICallback<CategoryDetails> callback);
	public abstract void addFeedToCategory(final String token, long c_id, long f_id, final APICallback<CategoryModel> callback);
	public abstract void delFeedFromCategory(final String token, long c_id, long f_id, final APICallback<CategoryModel> callback);
	public abstract void fetchCategoryFeeds(final String token, long c_id, final APICallback<List<FeedModel>> callback);
}
