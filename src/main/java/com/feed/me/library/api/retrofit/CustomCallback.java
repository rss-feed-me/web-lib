package com.feed.me.library.api.retrofit;

import com.feed.me.library.api.APICallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomCallback <T> implements Callback<T> {

	private final APICallback<T> apiCallback;
	public CustomCallback(APICallback<T> callback) {
		this.apiCallback = callback;
	}
	
	@Override
	public void onResponse(Call<T> call, Response<T> response) {
		apiCallback.onSuccess(response.code(), response.body());
	}

	@Override
	public void onFailure(Call<T> call, Throwable t) {
		apiCallback.onError(t);
	}
}
