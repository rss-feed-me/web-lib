package com.feed.me.library.test;



import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.feed.me.library.api.APICallback;
import com.feed.me.library.api.retrofit.APIService;
import com.feed.me.library.api.retrofit.RetrofitRSSAPI;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RetrofitImplemUnitTest {
	
	private static class UnitTestImplem extends RetrofitRSSAPI
	{

		protected UnitTestImplem(APIService service) {
			super(service);
		}
	}

	private final String token = "234213";
	private Call<Void> voidCall;
	private APIService apiService;
	private RetrofitRSSAPI rssAPI;
	
	@SuppressWarnings("unchecked")
	@BeforeEach
	public void setup_test()
	{
		voidCall = (Call<Void>) Mockito.mock(Call.class);
		apiService = Mockito.mock(APIService.class);
		rssAPI = new UnitTestImplem(apiService);
		Mockito.when(apiService.loginUser(Mockito.any())).thenReturn(voidCall);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void assert_login()
	{
		final APICallback<String> callback = (APICallback<String>) Mockito.mock(APICallback.class);
		Mockito.doAnswer(new Answer<Void>() {

			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				Callback<Void> callback = (Callback<Void>) invocation.getArguments()[0];
				callback.onResponse((Call<Void>) invocation.getMock(), Response.success(null, Headers.of("Authorization", token)));
				return null;
			}
		}).when(voidCall).enqueue(Mockito.any());
		
		rssAPI.login("mathias", "password", callback);
		Mockito.verify(callback, Mockito.times(1)).onSuccess(200, token);
		Assertions.assertEquals(token, rssAPI.getToken());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void assert_login_throw()
	{
		final APICallback<String> callback = (APICallback<String>) Mockito.mock(APICallback.class);
		Mockito.doAnswer(new Answer<Void>() {

			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				Callback<Void> callback = (Callback<Void>) invocation.getArguments()[0];
				callback.onFailure((Call<Void>) invocation.getMock(), new Throwable("Test throwable"));
				return null;
			}
		}).when(voidCall).enqueue(Mockito.any());
		rssAPI.login("mathias", "password", callback);
		Mockito.verify(callback, Mockito.times(1)).onError(Mockito.any());
		Assertions.assertNull(rssAPI.getToken());
	}
}
